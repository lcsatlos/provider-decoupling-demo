import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {BoldChatConfigModule} from "./modules/bold-chat-config/bold-chat-config.module";
import {GenesysConfigModule} from "./modules/genesys-config/genesys-config.module";
import {CommonConfigModule} from "./modules/common-config/common-config.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BoldChatConfigModule,
    GenesysConfigModule,
    CommonConfigModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
