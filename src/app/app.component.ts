import { Component } from '@angular/core';
import {CommonConfigService} from "./modules/common-config/common-config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'provider-decoupling-demo';

  constructor(configService: CommonConfigService) {
    console.log('common config returned value: ', configService.getConfigValue('some.config.path'));
  }
}
