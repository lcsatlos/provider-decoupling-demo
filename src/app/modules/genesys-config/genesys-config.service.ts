import { Injectable } from '@angular/core';

@Injectable()
export class GenesysConfigService {
  getConfig(configName: string): string | undefined {
    // @ts-ignore
    return Math.random() < 0.5 ? 'Dummy value provided by GenesysConfigService' : undefined;
  }
}
