import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CONFIG_SERVICE_TOKEN} from "../common-config/config-service.token";
import {GenesysConfigService} from "./genesys-config.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {provide: CONFIG_SERVICE_TOKEN, multi: true, useClass: GenesysConfigService}
  ]
})
export class GenesysConfigModule {
}
