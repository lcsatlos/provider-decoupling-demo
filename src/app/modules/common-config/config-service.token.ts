import {InjectionToken} from "@angular/core";

export const CONFIG_SERVICE_TOKEN = new InjectionToken('CONFIG_SERVICE_TOKEN');
