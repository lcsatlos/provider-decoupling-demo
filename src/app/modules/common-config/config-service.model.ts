export interface ConfigService {
  getConfigValue(configName: string): string | undefined;
}
