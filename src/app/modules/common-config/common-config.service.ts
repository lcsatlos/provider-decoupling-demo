import {Inject, Injectable} from '@angular/core';
import {CONFIG_SERVICE_TOKEN} from "./config-service.token";
import {ConfigService} from "./config-service.model";

@Injectable({
  providedIn: 'root'
})
export class CommonConfigService implements ConfigService {

  constructor(@Inject(CONFIG_SERVICE_TOKEN) private readonly configServices: ConfigService[]) {
    console.log('config services count: ', configServices.length);
  }

  getConfigValue(configName: string): string | undefined {
    for(let service of this.configServices) {
      const result = service.getConfigValue(configName);
      if(result !== undefined) {
        return result;
      }
    }
    return undefined;
  }
}
