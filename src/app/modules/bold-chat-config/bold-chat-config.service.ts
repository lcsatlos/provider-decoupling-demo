import { Injectable } from '@angular/core';
import {ConfigService} from "../common-config/config-service.model";

@Injectable()
export class BoldChatConfigService implements ConfigService {
  getConfigValue(configName: string): string | undefined {
    // @ts-ignore
    return Math.random() < 0.5 ? 'Dummy value provided by BoldChatConfigService' : undefined;
  }
}
