import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CONFIG_SERVICE_TOKEN} from "../common-config/config-service.token";
import {BoldChatConfigService} from "./bold-chat-config.service";

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {provide: CONFIG_SERVICE_TOKEN, multi: true, useClass: BoldChatConfigService}
  ]
})
export class BoldChatConfigModule {
}
